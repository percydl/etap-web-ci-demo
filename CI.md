# Continuous Integration Demo #

### A. Pre-flight instructions ###

1. Create an archive from latest etap-web. In my case, I have a local working copy at `~/temp/fresh-copy` 

```shell
$ cd ~/temp/fresh-copy
$ git archive web | gzip > ~/temp/etap-web.tar.gz
```

2. Set up a new repository sa bitbucket

3. In your local environment, push an empty master - we will simulate work like the real _etap_web_

```shell
$ mkdir continuous_integration\
$ cd continuous_integration
$ git init .
$ touch README.md
$ git commit -m "Initial commit"
$ git push 
```

4. In your local environment,  create a new branch

```shell
$ cd continuous_integration
$ git checkout -b web-develop
$ tar xzf ~/temp/etap-web.tar.gz
$ git add .
$ git commit -m "Initial commit for the branch"
```

### B. Setting up testing environment ###

1. Install **CIUnit** package. In your project root:

```shell
$ composer require Celc/ciunit dev-master
 ```
`vendor` should exists in your project root


2. Copy CIUnit's `tests` directory in to your project root

```shell
$ cp -R vendor/celc/ciunit/tests ./
```

4. Set up environment configuration. In your `application/config/database.php`:

```php

$db_host = '127.0.0.1';
$db_name = 'etap_web';
$db_user = 'test_user';
$db_pass = 'test_user_password';

$env_used = 'default';

if(defined('CIUnit_Version')) {
    $env_used = 'test';
}

$active_group = $env_used;

$active_record = TRUE;

$db['test']['hostname'] = $db_host;
$db['test']['username'] = $db_user;
$db['test']['password'] = $db_pass;
$db['test']['database'] = $db_name . "_test";
```

5. Delete or disable all sample tests

6. Run `phpunit`
```shell
$ cd tests
$ vendor/bin/phpunit
```

Note : 

We're using an old version of PHPUnit that can support CodeIgniter 2 :  Just ignore the message below:

```shell
PHP Error: Notice - Only variables should be passed by reference File Path: libraries/CIUnit.php (line: 62)
```

### Bitbucket Pipeline Configuration ###
```yaml

- step:
    name: 'Build and Test'
    image: php:7.4.27-apache-buster
    script:
      - echo "Your build and test goes here..."
      - apt update -y && apt -y install unzip curl mariadb-client && docker-php-ext-install mysqli
      - cp -p application/config/database-pipeline.php application/config/database.php
      - cat tests/schema/create_table_ut_users.sql | mysql -h 127.0.0.1 etap_web_test -u test_user -ptest_user_password
      - cd tests && ../vendor/bin/phpunit
    services:
      - mysql

definitions:
  services:
    mysql:
      image: mysql:5.7.34
      environment:
        MYSQL_DATABASE: 'etap_web_test'
        MYSQL_RANDOM_ROOT_PASSWORD: 'yes'
        MYSQL_USER: 'test_user'
        MYSQL_PASSWORD: 'test_user_password'
```
